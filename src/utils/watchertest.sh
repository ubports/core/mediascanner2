#!/bin/sh
#
# Copyright (C) 2024 Guido Berhoerster <guido+ubports@berhoerster.name>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of version 3 of the GNU General Public License as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

dbdir="${MEDIASCANNER_CACHEDIR:-${XDG_CACHE_HOME:-${HOME}/.cache}/mediascanner-2.0/}"
printf "This program simulates invalidation of query results as media files come and go.\n\n"
inotifywait -q --format '%e %f' -m -e create -e delete -e modify "${dbdir}" | \
    while read -r event name; do
        case $event in
        CREATE) printf 'Invalidation: create\n' ;;
        DELETE) printf 'Invalidation: delete\n' ;;
        MODIFY) printf 'Invalidation: modify %s\n' "${name}" ;;
        esac
    done

